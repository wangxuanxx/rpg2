package com.sts.mode;
/**
 * 
 * 封装用户信息实体类
 * 
 * **/
public class userinfo {

		private int id;
		private String name;
		private int pwd;
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public int getPwd() {
			return pwd;
		}
		public void setPwd(int pwd) {
			this.pwd = pwd;
		}
		
		
}
