package a.c.t;

import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class action extends ActionSupport {
		
	public String demo1(){
		//1.获取actioncontext
		ActionContext context = ActionContext.getContext();//从当前线程上获取
		//2.存入数据
		context.put("ContextMap","hello context map");
		//3.往应用域中存数据
		//第一种方式  servletapi 的方式
		ServletContext appliactionattr=ServletActionContext.getServletContext();
		appliactionattr.setAttribute("appliactionattr","hello appliaction attr");
		//第二种方式  
		Map<String,Object> appliactionMap=context.getApplication();
		appliactionMap.put("appliactionMap","hello appliaction map");
		//4.会话域 中存入数据
		HttpSession session =ServletActionContext.getRequest().getSession();
		session.setAttribute("sessionMap","hello session map");
		Map<String,Object> sessionattr=context.getSession();
		sessionattr.put("sessionAttr","hello session attr");
		return SUCCESS;
	}
}




//<!-- 配置struts2的常量区 -->
//<!-- 使用的编码格式为 utf-8 -->
//<!-- 该常量代表是否使用  struts2的一个非常强大的功能  叫    动态代理模式 -->
//<constant name="struts.enable.DynamicMethodInvocation" value="true"></constant>
//<constant name="struts.i18n.encoding" value="UTF-8"></constant>
//<!-- 代表是否使用开发者模式 以便于调式bug -->
//<constant name="struts.devMode" value="true"></constant>
//<!-- 代表你实现核心业务 所使用的 文件后缀名   action   do  没有 -->
//<constant name="struts.action.extension" value="action,do,"></constant>
//<constant name="struts.ognl.allowStaticMethodAccess" value="true"></constant>